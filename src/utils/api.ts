import constants from './constants.json';

const algolia_url = (query: string) => `'https://hn.algolia.com/api/v1/search?query=${query}`;
const bitbucket_url = () => `${constants.bitbucket_api_url}/${constants.repo}/?pagelen=100&page=1&sort=-created_on`;
const readme_url = (slug: string) => `${constants.bitbucket_api_url}/${constants.repo}/${slug}/src/master/README.md`;
const raw_url = (slug: string) => `${constants.bitbucket_url}/${constants.repo}/${slug}/raw/master`;

export type ErrorType = {
    error: {message: string};
}

export type ArticleType = {
    objectID: string;
    created_at: Date;
    author: string;
    url: string;
    title: string;
}

export function getAlgoliaArticles (setArticles: (a: ArticleType[]) => void,
                             setLoading: (b: boolean) => void,
                             setError: (e: Error) => void): void {
    setLoading(true);
    fetch(algolia_url('microfrontend'))
        .then(response => {
            setLoading(false);
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Something went wrong...');
            }
        })
        .then(data => setArticles(data.hits))
        .catch(error => setError(error));
}

export type BitBucketRepoType = {
    uuid: string;
    name: string;
    language: string;
    created_on: Date;
    author: string;
    owner: {display_name: string};
    slug: string;
    links: {source: {href: string}};
    description: string;
}

export function getBitBucketRepos (setRepos: (r: BitBucketRepoType[]) => void,
                             setLoading: (b: boolean) => void,
                             setError: (e: Error) => void): void {
    setLoading(true);
    fetch(bitbucket_url(),{
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            }
        })
        .then(response => {
            setLoading(false);
            if (response.ok) {
                return response.json();
            } else {
                throw response;
            }
        })
        .then(data => setRepos(data.values))
        .catch(error => setError(error));
}

export function getBitBucketReadme(slug: string,
                                   setReadme: (r: string) => void,
                                   setLoading: (b: boolean) => void,
                                   setError: (e: Error) => void): void {
    setLoading(true);
    fetch(readme_url(slug),{
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        }
    })
    .then(response => {
        setLoading(false);
        if (response.ok) {
            response.text().then(text => setReadme(replaceLocalLinks(text, slug)));
        } else {
            response.json().then(e => setError(e.error));
        }
    })
}

function replaceLocalLinks(str: string, slug: string) {
	return str.split('![image info](./assets').join('![image info](' + raw_url(slug) + '/assets');
}