import * as React from 'react';
import {Link} from 'react-router-dom';

import './header.css';

const Header = () => {
    return (
        <header className="App-header">
            <span className="App-logo">&gt;_</span>
            <Link to="/articles" className="App-title">code bites</Link>
            {/*<div className="App-links">*/}
            {/*    <Link to="/articles" className="App-link">home</Link>*/}
            {/*    <div className="divider"> | </div>*/}
            {/*    <Link to="/articles/new" className="App-link">new</Link>*/}
            {/*</div>*/}
        </header>
    );
};

export default Header;