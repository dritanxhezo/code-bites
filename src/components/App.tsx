import React from 'react';
import {Redirect, Route, Switch} from 'react-router';
import {BrowserRouter} from "react-router-dom";
import Header from "./header/header";
import ArticleList from './article-list/article-list';
import Article from './article/article';

import './App.css';

const App: React.FC = () => {
    return (
        <div className="app">
            <BrowserRouter>
                    <nav></nav>
                    <article>
                        <Header/>
                        <div className="app-main">
                        <Switch>
                            <Route exact path="/articles" component={ArticleList} />
                            <Route path="/articles/:id" component={Article} />
                            <Redirect from='/' to='/articles'/>
                        </Switch>
                        </div>
                        <footer className={"app-footer"}>CodeBites - Automatic blog generator from git Readme files | © Dritan Xhezo | Jan 2012 </footer>
                    </article>
                    <aside></aside>
            </BrowserRouter>
        </div>
    );
};

export default App;
