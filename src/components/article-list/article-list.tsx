import React, { useState, useEffect } from 'react';
//import { ArticleType, getAlgoliaArticles } from "../../utils/api";
import { BitBucketRepoType, getBitBucketRepos } from "../../utils/api";
import {Link} from "react-router-dom";

import './article-list.css';

const dateFormat = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

const ArticleSummary = (props: any) => {
    const created = new Date(props.article.created_on);
    return (
        <div className="articleSummary">
            <Link to={'/articles/' + props.article.slug}>
                <h2>{props.article.name}</h2>
                <p>{props.article.description}</p>
                <p>{props.article.owner.display_name} | {props.article.language} | {created.toLocaleDateString("en-US", dateFormat)}</p>
            </Link>
        </div>
        );
}

const ArticleList = () => {
    const [articles, setArticles] = useState(new Array<BitBucketRepoType>());
    const [isLoading, setLoading] = useState(false);
    const [error, setError] = useState();

    useEffect(() => {
        getBitBucketRepos(setArticles, setLoading, setError);
    }, []);

    if (error) {
        return <p>{error.message}</p>;
    }

    if (isLoading) {
        return <p>Loading ... </p>;
    }

    return (
        <div className={"articles"}>
            <ul>
                {articles.map((a: BitBucketRepoType) => <li key={a.uuid}><ArticleSummary article={a}/></li>)}
            </ul>
        </div>
    );
};

export default ArticleList;