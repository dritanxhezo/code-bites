import React, {useEffect, useState} from 'react';
import ReactMarkdown from "react-markdown";
import { getBitBucketReadme} from "../../utils/api";
import ReadingTime from "reading-time";

import './article.css';

const Article = (props: any) => {
    const [readme, setReadme] = useState();
    const [readingTime, setReadingTime] = useState();
    const [isLoading, setLoading] = useState(false);
    const [error, setError] = useState();

    useEffect(() => {
        getBitBucketReadme(props.match.params.id, setReadme, setLoading, setError);
    },  [props.match.params.id]);

    useEffect(() => {
    	if (readme && readme.length) {
        	setReadingTime(ReadingTime(readme).text);
        }
    },  [readme]);
    
    if (error) {
        return <p>{error.message}</p>;
    }

    if (isLoading) {
        return <p>Loading ... </p>;
    }

	
    return (
        <div className="article">
        	<span className="reading-time">Reading Time: {readingTime}</span>
            <ReactMarkdown source={readme} />
        </div>
    );
};

export default Article;
